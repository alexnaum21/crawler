'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Token = new Schema({
    tokenAddress: {
        type: String,
        index: true,
        unique: true
    },
    decimals: {
        type: Number
    },
    lastBlock: {
        type: Number
    },
    priceUsd: {
        type: Number
    },
    priceEth: {
        type: Number
    },
    priceBtc: {
        type: Number
    },
    holders: {
        type: Number
    },
    symbol: {
        type: String
    },
    finished: {
        type: Boolean
    },
    skip: {
        type: Boolean
    },
    description: {
        type: String
    }
}, { timestamps: false })

module.exports = mongoose.model('Token', Token)
