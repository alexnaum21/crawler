'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Holder = new Schema({
    tokenAddress: {
        type: String,
        index: true
    },
    holderAddress: {
        type: String,
        index: true,
    },
    balance: {
        type: Number
    },
    share: {
        type: Number
    }
}, { timestamps: false })

module.exports = mongoose.model('Holder', Holder)
