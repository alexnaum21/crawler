'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const WalletEvent = new Schema({
    address: {
        type: String,
        index: true,
        // unique: true
    },
    tokenAddress: {
        type: String,
        index: true,
        // unique: true
    },
    balance: {
        type: Number,
        index: true
    },
    transactionCount: {
        type: Number,
        index: true
    }
}, { timestamps: false })

module.exports = mongoose.model('WalletEvent', WalletEvent)
