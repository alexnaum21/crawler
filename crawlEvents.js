'use strict'

const config = require('config')
const db = require('./models/mongodb')
const q = require('./queues')
const web3 = require('./models/blockchain/chain')

let sleep = (time) => new Promise((resolve) => setTimeout(resolve, time))
const step = 1000
async function crawlProcess() {
  console.log('start')

  let tokens = await db.Token.find({
    $or: [
      {
        lastBlock: {
          $lt: config.get('toBlock')
        }
      },
      {lastBlock: null}
      ],
    symbol:'WAX'
  })
    .limit(1);

  // for( let tt = 0; tt < tokens.length; tt ++)
    for (let i = 4633000/*config.get('fromBlock')*/; i < config.get('toBlock'); i+=step) {

        /*
         TODO: Remove this check.
         It is only for Tomocoin. We break some first block because there is no transaction about Tomocoin event transfer
        */
        let eventsCommon = await Promise.all(tokens.map(token => {
          return web3.eth.getPastLogs({
            address: token.tokenAddress,
            topics: ["0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef"],
            fromBlock: i,
            toBlock:   i+step
          });
        }))

      let events = [];
      eventsCommon.forEach(ec => {
        events = events.concat(ec)
      })
      await Promise.all(tokens.map(token =>updateTokenBlock(token, i+ step)));
        /*if (config.get('tomoAddress') === '0x8b353021189375591723e7384262f45709a3c3dc'){
            if (i !== 5168958 && i !== 5169011 && i !== 5169173 && i < 5175169) {
                continue
            }
        }*/
        // if (i % 10 === 0) {
        //     console.log('Sleep 120 seconds')
        //     await sleep(120000)
        // }

       /* let block = await web3.eth.getBlock(i);
        let b = await db.Block.findOne({blockNumber: i})
        if (b && b.isFinish) {
            continue
        }
        if(!block.hash) continue
        await db.Block.findOneAndUpdate({blockNumber: block.number}, {
            hash: block.hash,
            blockNumber: block.number,
            transactionCount: block.transactions.length,
            parentHash: block.parentHash,
            timestamp: block.timestamp,
        }, { upsert: true, new: true })

        console.log("Process block number: " + i);*/
       console.log(`events length ${events.length} from ${i} to ${i+step}`)
        let listTransactions = events
        if (listTransactions != null && listTransactions.length) {
            await q.create('newEvent', {events: JSON.stringify(listTransactions)})
                .attempts(5).backoff({delay: 5000})
                .priority('low').removeOnComplete(true).save()
        }
    }
}

crawlProcess()
const updateTokenBlock = async (token, block) => {
  token.lastBlock = block;
  await token.save();
  return true;
}