'use strict'

const config = require('config')
const db = require('./models/mongodb')
// const q = require('./queues')
const web3 = require('./models/blockchain/chain')
var requestify = require('requestify');
let sleep = (time) => new Promise((resolve) => setTimeout(resolve, time))
const step = 1000
async function crawlProcess() {
  console.log('start')

  let tokens = await db.Token.find({
    /*$or: [
      {
        lastBlock: {
          $lt: config.get('toBlock')
        }
      },
      {lastBlock: null}
      ],*/
    tokenAddress:"0x0d8775f648430679a709e98d2b0cb6250d2887ef",
   finished:{$ne:true},
    skip:{$ne:true},
  })
    .limit(1);
  for(let j = 0; j < tokens.length; j ++){
    await processToken(tokens[j]);
  }

  // for( let tt = 0; tt < tokens.length; tt ++)
  // let token = tokens[0]
   return true
}
const processToken = async token => {
  for (let i = 499/*config.get('fromBlock')*/; i < 5000; i++) {

    try{
      console.log(`page ${i} token ${token.tokenAddress}`)
      let resp = await requestify.get(`https://ethplorer.io/service/service.php?refresh=holders&data=${token.tokenAddress}&page=tab%3Dtab-holders%26holders%3D${i}%26pageSize%3D100&showTx=all`);
      let body = JSON.parse(resp.getBody());
      console.log(/*body,*/ body.holders.length);
      if(!body.holders){
        token.skip = true;
        await token.save();
        return true;
      }
      if(body.holders.length === 0) break;
      await Promise.all(body.holders.map(h => saveHolder(token.tokenAddress, h)))
      await sleep(2000)
    }catch (e) {
      console.log(e);
      await sleep(6000)
      i--;

    }
  }
  token.finished = true;
  await token.save();
  return  true;
}

crawlProcess()
const updateTokenBlock = async (token, block) => {
  token.lastBlock = block;
  await token.save();
  return true;
}
const saveHolder = async (tokenAddress, holder) => {
  try{
    let exists = await db.Holder.findOne({tokenAddress: tokenAddress, holderAddress: holder.address.toLowerCase()});
    if(exists){
      console.log(holder.address, 'exists');
      exists.balance = holder.balance;
      exists.share = holder.share;
      await exists.save();
      return true;
    }else{
      await db.Holder.create({
        holderAddress:holder.address.toLowerCase(),
        balance:holder.balance,
        share:holder.share,
        tokenAddress:tokenAddress.toLowerCase(),
      })
      return true
    }
  }catch (e) {
    throw e;
  }

}
