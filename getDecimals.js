
const db = require('./models/mongodb')
// let rpcUrl = "https:/mainnet.infura.io/v3/3c6e8a49ffb447deb8c12c70c7153329";
let rpcUrl = "https://mainnet.infura.io/v3/f482dde9f1f249a392e81f6c95d6ecd2";
const Web3 = require('web3')
var web3 = new Web3(new Web3.providers.HttpProvider(rpcUrl));
const erc20ABI = [
  {
    "constant": true,
    "inputs": [],
    "name": "name",
    "outputs": [
      {
        "name": "",
        "type": "string"
      }
    ],
    "payable": false,
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "decimals",
    "outputs": [
      {
        "name": "",
        "type": "uint8"
      }
    ],
    "payable": false,
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [
      {
        "name": "_owner",
        "type": "address"
      }
    ],
    "name": "balanceOf",
    "outputs": [
      {
        "name": "balance",
        "type": "uint256"
      }
    ],
    "payable": false,
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "symbol",
    "outputs": [
      {
        "name": "",
        "type": "string"
      }
    ],
    "payable": false,
    "type": "function"
  }
];

const processData = async () => {
  try{
    let tokens = await db.Token.find();
// await getTokenDec(tokens[0])
    await Promise.all(tokens.map(updateTokenAddr));
    return true
  }catch (e) {
    throw e;
  }
};

const getTokenDec = async token => {
  var tokenContract = new web3.eth.Contract(erc20ABI,token.tokenAddress)
  // console.log(token.tokenAddress)
  var decimal =  await tokenContract.methods.decimals().call();
  token.decimals = decimal;
  await token.save();

  return true
}
const updateTokenAddr = async token => {
  // var tokenContract = new web3.eth.Contract(erc20ABI,token.tokenAddress)
  // console.log(token.tokenAddress)
  // var decimal =  await tokenContract.methods.decimals().call();
  token.tokenAddress = token.tokenAddress.toLowerCase();
  await token.save();

  return true
}

processData().then(console.log).catch(console.error)