'use strict'
const db = require('../models/mongodb')
const consumer = {}

consumer.name = 'addAmountToWalletEvent'

consumer.task = async function(job, done) {
    let fromWallet = job.data.fromWallet
    let toWallet = job.data.toWallet

    let tokenAddress = job.data.tokenAddress
    let tokenAmount = job.data.tokenAmount
    let transactionHash = job.data.transactionHash
    console.log('   - add ', tokenAmount, 'TOMO to wallet: ', toWallet)
    let wallet = await db.WalletEvent.findOneAndUpdate({address: toWallet, tokenAddress: tokenAddress}, {address: toWallet, tokenAddress: tokenAddress}, { upsert: true, new: true })
    if (wallet.balance) {
        wallet.balance += tokenAmount
    } else {
        wallet.balance = tokenAmount
    }
    if (wallet.transactionCount) {
        wallet.transactionCount += 1
    } else {
        wallet.transactionCount = 1
    }

    wallet.save()

    await db.TransactionEvent.findOneAndUpdate(
        {hash: transactionHash, fromAccount: fromWallet, toAccount: toWallet},
        {isAddToken: true}, { upsert: true, new: true }
    )

    done();

};

module.exports = consumer